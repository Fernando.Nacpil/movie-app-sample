package com.tmdb.movieapp.common

open class Screen(val name : String) {
    fun withArgs(vararg args : String) : String {
        return buildString {
            append(name)
            args.forEach { arg ->
                append("/$arg")
            }
        }
    }
}