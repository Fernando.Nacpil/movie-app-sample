package com.tmdb.movieapp.common

object Constants {
    const val BASE_URL = "https://api.themoviedb.org/3/movie/"
    const val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500/"
    const val CONNECT_TIMEOUT = 15L
    const val WRITE_TIMEOUT = 15L
    const val READ_TIMEOUT = 15L
}