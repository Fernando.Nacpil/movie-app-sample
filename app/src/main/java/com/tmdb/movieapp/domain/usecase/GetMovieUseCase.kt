package com.tmdb.movieapp.domain.usecase

import com.tmdb.movieapp.common.Resource
import com.tmdb.movieapp.data.dto.MovieDetailResponse
import com.tmdb.movieapp.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow

class GetMovieUseCase (
    private val movieRepository: MovieRepository
)
{
    suspend operator fun invoke(id : Int) : Flow<Resource<MovieDetailResponse>> {
        return movieRepository.getMovie(id)
    }
}