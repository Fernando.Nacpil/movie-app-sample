package com.tmdb.movieapp.domain.repository

import com.tmdb.movieapp.common.Resource
import com.tmdb.movieapp.data.dto.MovieDetailResponse
import com.tmdb.movieapp.data.dto.MovieResponse
import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    suspend fun getMovies(pageNumber: Int) : Flow<Resource<MovieResponse>>
    suspend fun getMovie(id : Int) : Flow<Resource<MovieDetailResponse>>
}