package com.tmdb.movieapp.domain.usecase

import com.tmdb.movieapp.common.Resource
import com.tmdb.movieapp.data.dto.MovieResponse
import com.tmdb.movieapp.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow

class GetMoviesUseCase (
    private val movieRepository: MovieRepository
)
{
    suspend operator fun invoke(pageNumber : Int) : Flow<Resource<MovieResponse>> {
        return movieRepository.getMovies(pageNumber)
    }
}