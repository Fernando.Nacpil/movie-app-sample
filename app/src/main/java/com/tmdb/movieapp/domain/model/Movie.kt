package com.tmdb.movieapp.domain.model

data class Movie(
    val id : Int,
    val poster_path : String,
    val title : String
)
