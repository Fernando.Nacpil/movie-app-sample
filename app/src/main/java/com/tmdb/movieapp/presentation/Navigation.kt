package com.tmdb.movieapp.presentation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.input.key.Key.Companion.Home
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.tmdb.movieapp.presentation.movie.MovieDetail
import com.tmdb.movieapp.presentation.movie.MovieDetailScreen
import com.tmdb.movieapp.presentation.movielist.MovieList
import com.tmdb.movieapp.presentation.movielist.MovieListScreen
import org.koin.androidx.compose.getViewModel

@Composable
fun Navigation() {
    val navigation = rememberNavController()
    NavHost(navController = navigation, startDestination = MovieList.ROUTE.name) {
        composable(route = MovieList.ROUTE.name) {
            MovieListScreen(onNavigateToMovie = {
                navigation.navigate(MovieDetail.ROUTE.withArgs(it))
            })
        }
        composable(route = MovieDetail.ROUTE.name + "/{movieId}",
        arguments = listOf(
            navArgument("movieId") {
                type = NavType.IntType
            }
        )) { entry ->
            entry.arguments?.getInt("movieId")?.let { MovieDetailScreen(movieId = it) }
        }
    }
}