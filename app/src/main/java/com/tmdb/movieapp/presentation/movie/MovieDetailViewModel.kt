package com.tmdb.movieapp.presentation.movie

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.tmdb.movieapp.common.Constants
import com.tmdb.movieapp.common.Resource
import com.tmdb.movieapp.data.dto.MovieDetailResponse
import com.tmdb.movieapp.domain.usecase.GetMovieUseCase
import com.tmdb.movieapp.presentation.base.BaseViewModelImpl
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class MovieDetailViewModel (
    id : Int,
    private val getMovieUseCase: GetMovieUseCase
) : BaseViewModelImpl() {

    private var _stateFlow = MutableStateFlow(MovieDetailState())
    val stateFlow: StateFlow<MovieDetailState> = _stateFlow

    init {
        getMovie(id)
    }

    private fun getMovie(id : Int) {
        viewModelScope.launch {
            getMovieUseCase(id).collect { resource ->
                when (resource) {
                    is Resource.Success -> {
                        _stateFlow.value = MovieDetailState(
                            movie = resource.data?.apply {
                                poster_path = Constants.IMAGE_BASE_URL + poster_path
                            } ?: MovieDetailResponse(),
                            isLoading = false)
                        Log.d("movies","movies: " + resource.data?.title)
                    }
                    is Resource.Error -> {
                        _stateFlow.update {
                            _stateFlow.value.copy(error = resource.message ?: "An unexpected error occurred.", isLoading = false)
                        }
                    }
                    is Resource.Loading -> {
                        _stateFlow.update {
                            _stateFlow.value.copy(isLoading = true)
                        }
                    }
                }
            }
        }
    }
}