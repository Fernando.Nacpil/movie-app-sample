package com.tmdb.movieapp.presentation.movie

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.lifecycle.ViewModel
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.tmdb.movieapp.common.Screen
import com.tmdb.movieapp.presentation.movielist.MovieListViewModel
import org.koin.androidx.compose.get
import org.koin.androidx.compose.getViewModel
import org.koin.androidx.compose.koinViewModel
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.parameter.parametersOf
import org.koin.core.qualifier.Qualifier

class MovieDetail {
    object ROUTE : Screen("movie_detail")
}

@Composable
fun MovieDetailScreen(movieId: Int, vm : MovieDetailViewModel = get{ parametersOf(movieId) }){
    val state by vm.stateFlow.collectAsState()
    Card(modifier = Modifier
        .padding(54.dp, 54.dp)
        .fillMaxWidth()
        .fillMaxHeight(),
        shape = RoundedCornerShape(8.dp),
        elevation = 4.dp,
        backgroundColor = Color.Transparent
    ){
        Row (Modifier.padding(4.dp)
            .fillMaxSize()){
            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(state.movie.poster_path)
                    .crossfade(true)
                    .build(),
                contentDescription =  state.movie.overview,
                contentScale = ContentScale.Crop,
                modifier = Modifier.clip(CircleShape)
                    .weight(0.2f)
                    .width(100.dp)
                    .height(100.dp)
            )
            Column (verticalArrangement = Arrangement.Top,
                modifier = Modifier.padding(4.dp)
                    .fillMaxHeight()
                    .weight(0.8f)){
                Text(text = state.movie.title,
                    style = MaterialTheme.typography.h5)
                Text(text = state.movie.overview,
                    style = MaterialTheme.typography.subtitle2)
                Text(text = state.movie.vote_average.toString(),
                    style = MaterialTheme.typography.h6)
            }

        }
    }

}

