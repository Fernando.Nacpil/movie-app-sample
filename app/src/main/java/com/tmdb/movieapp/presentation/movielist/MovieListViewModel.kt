package com.tmdb.movieapp.presentation.movielist

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.tmdb.movieapp.common.Resource
import com.tmdb.movieapp.domain.usecase.GetMovieUseCase
import com.tmdb.movieapp.domain.usecase.GetMoviesUseCase
import com.tmdb.movieapp.presentation.base.BaseViewModelImpl
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class MovieListViewModel (
    private val getMoviesUseCase: GetMoviesUseCase,
    private val getMovieUseCase: GetMovieUseCase
) : BaseViewModelImpl() {

    private var _stateFlow = MutableStateFlow(MovieListState())
    val stateFlow: StateFlow<MovieListState> = _stateFlow
    private var totalPage = 0
    private var currentPage = 1

    init {
        getMovies()
    }

    fun getMovies() {
        viewModelScope.launch {
            _stateFlow.update {
                _stateFlow.value.copy(isLoading = true)
            }

            getMoviesUseCase(getCurrentPage())
                .collect { resource ->

                when (resource) {
                    is Resource.Success -> {
                        _stateFlow.update {
                            _stateFlow.value.copy(
                                results = _stateFlow.value.results + (resource.data?.results
                                    ?: emptyList()),
                                isLoading = false
                            )
                        }
                        totalPage = resource.data?.total_pages ?: 0
                    }
                    is Resource.Error -> {
                        _stateFlow.update {
                            _stateFlow.value.copy(
                                error = resource.message ?: "An unexpected error occurred.",
                                isLoading = false
                            )
                        }
                    }
                    is Resource.Loading -> {
                        _stateFlow.update {
                            _stateFlow.value.copy(isLoading = true)
                        }
                    }

                }
            }
        }
    }
    private fun getCurrentPage() : Int{
        if (totalPage > 0 && currentPage <= totalPage) {
            currentPage++
        }
        return  currentPage
    }

    fun getMovie(id : Int) {
        viewModelScope.launch {
            getMovieUseCase(id).collect { resource ->
                when (resource) {
                    is Resource.Success -> {
                        Log.d("movies","movies: " + resource.data?.title)
                    }
                    is Resource.Loading -> {

                    }
                    is Resource.Error -> {

                    }
                }
            }
        }
    }
}



//    override fun test() {
//        TODO("Not yet implemented")
//    }
//}