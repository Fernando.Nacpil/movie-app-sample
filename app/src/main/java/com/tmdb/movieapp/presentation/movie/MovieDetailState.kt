package com.tmdb.movieapp.presentation.movie

import androidx.compose.runtime.Stable
import com.tmdb.movieapp.data.dto.MovieDetailResponse

@Stable
data class MovieDetailState(
    val isLoading: Boolean = false,
    val movie : MovieDetailResponse = MovieDetailResponse(),
    val error: String? = null
)