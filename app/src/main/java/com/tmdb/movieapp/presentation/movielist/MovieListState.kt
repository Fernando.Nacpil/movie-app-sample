package com.tmdb.movieapp.presentation.movielist

import androidx.compose.runtime.Stable
import com.tmdb.movieapp.data.dto.Result

@Stable
data class MovieListState(
    val isLoading: Boolean = false,
    val results: List<Result> = emptyList(),
    val error: String? = null
)