package com.tmdb.movieapp.presentation.movielist

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.tmdb.movieapp.common.Screen
import com.tmdb.movieapp.data.dto.Result
import org.koin.androidx.compose.get

class MovieList {
    object ROUTE : Screen("movie_list")
}

@Composable
fun MovieItem(result: Result, modifier: Modifier){
    Card(modifier = modifier
        .padding(8.dp, 4.dp)
        .fillMaxWidth()
        .height(110.dp),
    shape = RoundedCornerShape(8.dp),
        elevation = 4.dp
    ){
        Row (Modifier.padding(4.dp)
            .fillMaxSize()){
            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(result.poster_path)
                    .crossfade(true)
                    .build(),
                contentDescription =  result.title,
                contentScale = ContentScale.Crop,
                modifier = Modifier.clip(CircleShape)
                    .weight(0.2f)
            )

            Column (verticalArrangement = Arrangement.Center,
                modifier = Modifier.padding(4.dp)
                    .fillMaxHeight()
                    .weight(0.8f)){
                result.title?.let {
                    Text(text = it,
                        style = MaterialTheme.typography.h6)
                }
            }

        }

    }
}
data class ScrollContext(
    val isBottom: Boolean,
)
@Composable
fun rememberScrollContext(listState: LazyListState): ScrollContext {
    val scrollContext by remember {
        derivedStateOf {
            ScrollContext(
                isBottom = listState.isLastItemVisible
            )
        }
    }
    return scrollContext
}

val LazyListState.isLastItemVisible: Boolean
    get() = layoutInfo.visibleItemsInfo.lastOrNull()?.index == layoutInfo.totalItemsCount - 1

@Composable
fun MovieListScreen(onNavigateToMovie: (id : String) -> Unit, vm : MovieListViewModel = get()) {

    val state by vm.stateFlow.collectAsState()
    val listState = rememberLazyListState()
    val scrollContext = rememberScrollContext(listState)

    if(scrollContext.isBottom && !state.isLoading) {
        vm.getMovies()
    }

    LazyColumn (state = listState, modifier = Modifier.fillMaxSize()) {
        items(state.results, key = { movie ->
            movie.id
        }) { movie ->
            MovieItem(movie, modifier = Modifier.clickable {
                onNavigateToMovie(movie.id.toString())
            })
        }
        item {
            if (state.isLoading){
                Row(modifier = Modifier.fillMaxWidth()
                    .padding(8.dp),
                    horizontalArrangement = Arrangement.Center){
                    CircularProgressIndicator()
                }
            }
        }
    }
}
