package com.tmdb.movieapp

import android.app.Application
import com.tmdb.movieapp.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MovieApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            modules(apiModule, repositoryModule, RetrofitModule , viewModelModule )
            androidLogger()
            androidContext(this@MovieApplication)
        }
    }
}