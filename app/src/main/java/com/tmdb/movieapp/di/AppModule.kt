package com.tmdb.movieapp.di

import com.tmdb.movieapp.data.local.repository.MovieLocalImpl
import com.tmdb.movieapp.data.remote.MovieApi
import com.tmdb.movieapp.data.remote.repository.MovieRepositoryImpl
import com.tmdb.movieapp.domain.repository.MovieRepository
import com.tmdb.movieapp.domain.usecase.GetMovieUseCase
import com.tmdb.movieapp.domain.usecase.GetMoviesUseCase
import com.tmdb.movieapp.presentation.base.BaseViewModelImpl
import com.tmdb.movieapp.presentation.movie.MovieDetailViewModel
import com.tmdb.movieapp.presentation.movielist.MovieListViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit


val apiModule = module {
    single(createdAtStart = false) { get<Retrofit>().create(MovieApi::class.java) }
}

val repositoryModule = module {
    singleOf(::MovieRepositoryImpl) bind MovieRepository::class
//    singleOf(::MovieLocalImpl) bind MovieRepository::class
    singleOf(::GetMoviesUseCase)
    singleOf(::GetMovieUseCase)
}

val viewModelModule = module {
    viewModelOf(::BaseViewModelImpl)
    viewModelOf(::MovieListViewModel)
    viewModelOf(::MovieDetailViewModel)
}