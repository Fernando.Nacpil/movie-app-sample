package com.tmdb.movieapp.data.local.repository

import android.content.Context
import android.util.Log
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.tmdb.movieapp.common.Resource
import com.tmdb.movieapp.data.dto.MovieDetailResponse
import com.tmdb.movieapp.data.dto.MovieResponse
import com.tmdb.movieapp.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.io.IOException
import java.io.InputStream

class MovieLocalImpl(private val moshi: Moshi, private val context : Context) : MovieRepository{
    override suspend fun getMovies(pageNumber: Int): Flow<Resource<MovieResponse>> = flow {
        val moviesJson = getMovieJSON()
//        val listType = Types.newParameterizedType(List::class.java, MovieDto::class.java)
        val adapter: JsonAdapter<MovieResponse> = moshi.adapter(MovieResponse::class.java)
        val result = adapter.fromJson(moviesJson)
        result?.let { movies ->
            emit(Resource.Success(movies))
        }

    }

    override suspend fun getMovie(id: Int): Flow<Resource<MovieDetailResponse>> {
        TODO("Not yet implemented")
    }


    private fun getMovieJSON(fileName : String = "popular_movies_list.json"): String {
        val inputStream = getInputStreamForJsonFile(fileName)
        return inputStream.bufferedReader().use { it.readText() }
    }

    @Throws(IOException::class)
    internal open fun getInputStreamForJsonFile(fileName: String): InputStream {
        Log.d("movie", "reading file: $fileName")
        return context.assets.open(fileName)
    }
}