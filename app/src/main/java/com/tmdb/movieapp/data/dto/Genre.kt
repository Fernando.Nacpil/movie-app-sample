package com.tmdb.movieapp.data.dto

data class Genre(
    val id: Int? = 0,
    val name: String? = ""
)