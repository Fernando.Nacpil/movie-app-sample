package com.tmdb.movieapp.data.dto

data class BelongsToCollection(
    val backdrop_path: String? = "",
    val id: Int? = 0,
    val name: String? = "",
    val poster_path: String? = ""
)