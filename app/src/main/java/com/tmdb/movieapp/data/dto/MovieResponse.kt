package com.tmdb.movieapp.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MovieResponse(
    val page: Int,
    @Json(name = "results") val results: List<Result>,
    val total_pages: Int,
    val total_results: Int
)