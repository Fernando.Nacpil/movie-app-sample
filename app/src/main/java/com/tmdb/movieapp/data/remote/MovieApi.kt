package com.tmdb.movieapp.data.remote

import com.haroldadmin.cnradapter.NetworkResponse
import com.tmdb.movieapp.BuildConfig
import com.tmdb.movieapp.common.Constants
import com.tmdb.movieapp.data.dto.ErrorResponse
import com.tmdb.movieapp.data.dto.MovieDetailResponse
import com.tmdb.movieapp.data.dto.MovieResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApi {
    @GET("top_rated")
    suspend fun getMovies(
        @Query("page")
        page : Int = 1,
        @Query("api_key")
        api_key: String = BuildConfig.API_KEY,
        @Query("language")
        language: String = "en-US"
    ) : NetworkResponse<MovieResponse, ErrorResponse>

    @GET("{id}")
    suspend fun getMovie(
        @Query("id") id : Int,
        @Query("api_key")
        api_key: String = BuildConfig.API_KEY,
    ) : NetworkResponse<MovieDetailResponse,ErrorResponse>
}