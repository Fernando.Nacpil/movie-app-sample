package com.tmdb.movieapp.data.remote.repository

import com.haroldadmin.cnradapter.NetworkResponse
import com.tmdb.movieapp.common.Constants.IMAGE_BASE_URL
import com.tmdb.movieapp.common.Resource
import com.tmdb.movieapp.data.remote.MovieApi
import com.tmdb.movieapp.data.dto.MovieDetailResponse
import com.tmdb.movieapp.data.dto.MovieResponse
import com.tmdb.movieapp.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow


class MovieRepositoryImpl (
    private val api: MovieApi
): MovieRepository {
    override suspend fun getMovies(pageNumber : Int): Flow<Resource<MovieResponse>>  = flow {
        when (val response = api.getMovies(pageNumber)) {
            is NetworkResponse.Success -> emit(Resource.Success(response.body.apply {
                results?.onEach { movie ->
                    movie.poster_path= IMAGE_BASE_URL + movie.poster_path
                }
            }))
            is NetworkResponse.Error -> emit(Resource.Error(response.body?.message ?: "network error" ))
        }
    }

    override suspend fun getMovie(id: Int): Flow<Resource<MovieDetailResponse>>  = flow {
        when (val response = api.getMovie(id)) {
            is NetworkResponse.Success -> emit(Resource.Success(response.body))
            is NetworkResponse.Error -> emit(Resource.Error(response.body?.message ?: "network error" ))
        }
    }
}