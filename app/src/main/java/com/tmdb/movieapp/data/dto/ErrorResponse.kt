package com.tmdb.movieapp.data.dto

data class ErrorResponse(val message: String)